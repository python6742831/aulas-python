#Crie um módulo chamado "data_hora" que contenha uma 
# função para retornar a data e hora atuais. Em seguida, 
# crie um programa principal que importe esse módulo e 
# imprima a data e hora.

from modulos import data_hora

print(f"Data e Hora atual: {data_hora.dataHoraAtual()}")