#Suponha que em Codeville temos uma 
# classe para representar contas bancárias 
# com informações recebidas. Vamos aplicar 
# o encapsulamento usando os níveis de acesso.

class ContaBancaria:
    def __init__(self, titular, saldo):
        self.public_titular = titular
        self._protected_saldo = saldo
        self.__private_codigo_acesso = "1234"

    def verificar_saldo(self):
        print(f"Saldo disponível: R$ {self._protected_saldo}")

    def __acessar_conta(self):
        print(f"Conta autorizada")

conta = ContaBancaria("João", 2000)
conta.verificar_saldo()