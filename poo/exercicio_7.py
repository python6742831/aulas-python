#Na cidade de Codeville, há diferentes meios de transporte. 
# Crie uma classe base MeioDeTransporte com o método mover. 
# Crie subclasses Carro, Barco e 
# Aviao que herdam da classe base 
# e implementam o método de maneiras diferentes.

class MeioDeTransporte:
    def mover(self):
        pass

class Carro(MeioDeTransporte):
    def mover(self):
        print("O carro anda na estrada")

class Barco(MeioDeTransporte):
    def mover(self):
        print(f"O barco se locomove na água")

class Aviao(MeioDeTransporte):
    def mover(self):
        print("O avião se locomove no céu")

carro = Carro()
barco = Barco()
aviao = Aviao()

meios = [carro, barco, aviao]

for meio in meios:
    meio.mover()