"""
 Uma loja de roupas está fazendo uma promoção onde oferece 
 descontos de 10%  para compras acima de R$ 200,00. 
 Escreva um programa em Python que receba o valor total 
 da compra e calcule o valor final com desconto, se houver.
"""

valor_total = float(input("Digite o valor total da compra: "))

if valor_total > 200:
    valor_desconto = valor_total * 0.1
    valor_final = valor_total - valor_desconto
    print(f"O valor com desconto é de R$ {valor_final:.2f}")