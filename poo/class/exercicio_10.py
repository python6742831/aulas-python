"""
Crie uma classe chamada Livro que tenha os 
atributos título e autor. Em seguida, crie um método
chamado detalhes que imprima os detalhes do livro (título e autor)
"""

titulo = input("Informe o nome do livro: ")
autor = input("Informe o nome do autor: ")

class Livro:
    def __init__(self, titulo, autor):
        self.titulo = titulo
        self.autor = autor
    
    def detalhes(self):
        print(f"O livro {self.titulo} foi escrito pelo autor {self.autor}")

livro = Livro(titulo, autor)
livro.detalhes()
