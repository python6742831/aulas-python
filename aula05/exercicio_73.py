#A NASA está desenvolvendo um software para simular o pouso lunar. 
# Crie um  programa que verifica se a altitude de um módulo lunar é segura para o pouso. 
# Ela tem que ser menor que 50

import random

altitude = random.randint(1, 100) #random faz sorteio de números aleatórios

if altitude <= 50:
    print("A altitude de um módulo lunar é segura para pouso.")
else:
    print("A altitude ainda não está segura, o valor deve ser abaixo de 50.")
