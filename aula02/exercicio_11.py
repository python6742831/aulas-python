#Faça um programa que peça o nome do usuário e verifique se ele é "admin", 
# ele é admin ser for Lucas ou Pedro.

#Receber o nome do usário
nome = input("Digite seu nome: ").upper()

if nome == "LUCAS" or nome == "PEDRO":
    print("Bem-vindo, admin!")
else:
    print(f"Bem vindo, {nome}")