#EXEMPLO DE OPERADORES LÓGICOS
x = 10
y = 5

if x > 5 and y < 10:
    print("Os dois testes são verdadeiros")

if x > 5 or y < 2:
    print("Pelo menos um dos testes é verdadeiro")

if not x < y:
    print("x não é menor que y")

#EXEMPLO DE OPERADORES ARITMÉTICOS
a = 10
b = 3

print(a + b) #SOMA
print(a - b) #SUBTRAÇÃO
print(a * b) #MULTIPLICAÇÃO
print(a / b) #DIVISÃO
print(a // b) #DIVISÃO INTEIRA
print(a % b) #RESTO DA DIVISÃO
print(a ** b) #EXPONENCIAÇÃO

#EXEMPLO DE OPERADORES DE COMPARAÇÃO
x = 10
y = 5

if x == y:
    print("x é igual a y")

if x != y:
    print("x é diferente de y")

if x > y:
    print("x é maior que y")

if x < y:
    print("x é menor que y")

if x >= y:
    print("x é maior ou igual a y")

if x <= y:
    print("x é menor ou igual a y")
