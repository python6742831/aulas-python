"""
Crie uma classe chamada Triangulo que tenha
os atributos base e altura. Em seguida, crie um método
chamadao area que retorne a área do triângulo 
(área = base * altura / 2)
"""

class Triangulo:
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def areaTriangulo(self):
        area = (self.base * self.altura) / 2
        print(f"Base do triângulo: {self.base} \n Altura do triângulo: {self.altura} \n Área do triângulo: {area}")

triangulo1 = Triangulo(10, 12)
triangulo1.areaTriangulo()