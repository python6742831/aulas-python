# Desenvolva um programa que conte quantas vezes a palavra
#  "comentário" foi mencionada em uma seção de comentários
# de uma publicação no LinkedIn, utilizando um loop for.

comments = ["Ótimo comentário!", "Concordo com o comentário anterior.",
"Esse comentário é muito relevante.", "comentário interessante!"]

comentario = 0
count_comentario = 0

while comentario < len(comments):
    count_comentario += comments[comentario].lower().count('comentário')
    comentario += 1

print(f"Foram encontrados {count_comentario} palavras 'comentário'.")