"""
Crie uma classe chamada BonecaBarbie que tenha
os atributos nome, cor_cabelo e acessórios. Em
seguida, crie um método chamado mostrar_informacoes
que imprima as informações da boneca (nome, cor do cabelo e acessórios)
"""

class BonecaBarbie:
    def __init__(self, nome, cor_cabelo, acessorios ): #método de inicialização da classe, dentro do método é adicionado os atributos. Primeiro parâmetro sempre será o self, é o objeto em si
        self.nome = nome #self é o objeto instânciado
        self.cor_cabelo = cor_cabelo
        self.acessorios = acessorios

    def mostrar_informacoes(self): #função para receber o obejto instânciado
        print(f"Boneca: {self.nome}, cor de cabelo: {self.cor_cabelo}, acessórios: {self.acessorios}")

barbie1 = BonecaBarbie("Barbie Fashion", "loiro", "colar") #criar objeto, barbie1 é igual ao self
barbie2 = BonecaBarbie("Barbie Maluca", "castanho", "pulseira")

barbie1.mostrar_informacoes()
barbie2.mostrar_informacoes()

#foram usado dois objetos = barbie1 e barbie2 usando o mesmo molde = BonecaBarbie