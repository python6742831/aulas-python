'''
Faça um programa para leitura de duas notas parciais por aluno. O programa deve calcular a média 
alcançada por aluno e apresentar: 
A mensagem "Aprovado", se a média for maior ou igual a 7;
A mensagem "Reprovado", se a média for menor do que 7;
A mensagem "Aprovado com Louvor", se a média for igual a 10.
'''

nota1 = float(input("Digite a primeira nota: \n"))
nota2 = float(input("Digite a segunda nota: \n"))

media = (nota1 + nota2) / 2
print(f"A média deste aluno é: {media}") #f é usado como uma concatenação para imprimir na mensagem o valor de uma variável

if media == 10:
    print("Aprovado com Louvor")
elif media >= 7:
    print("Aprovado")
else:
    print("Reprovado")