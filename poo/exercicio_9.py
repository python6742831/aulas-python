#Na escola de música Codeville, há vários
# instrumentos diferentes. 
# Crie uma classe base Instrumento Musical com 
# o método tocar. Crie subclasses Violino, Flauta 
# e Bateria que herdam da classe base e 
# implementam o método de maneiras diferentes.

class IntrumentoMusical:
    def tocar(self):
        pass

class Violino(IntrumentoMusical):
    def tocar(self):
        print("Violino tocando")

class Flauta(IntrumentoMusical):
    def tocar(self):
        print(f"Flauta tocando")

class Bateria(IntrumentoMusical):
    def tocar(self):
        print(f"Bateria tocando")

violino = Violino()
flauta = Flauta()
bateria = Bateria()

sons = [violino, flauta, bateria]

for som in sons:
    som.tocar()