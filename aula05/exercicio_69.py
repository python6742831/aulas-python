#Crie uma função chamada `contar_likes` que receba 
# uma lista de posts de uma rede social 
# e retorne a quantidade total de likes desses posts.

posts = [
    {'id': 1, 'likes': 10},
    {'id': 2, 'likes': 5},
    {'id': 3, 'likes': 15}
]

def contar_likes(posts):
    total_likes = 0

    for post in posts:
        total_likes += post["likes"]
    return total_likes  

print(contar_likes(posts))