#Crie uma função chamada `adicionar_amigo` que receba uma
# lista de amigos de um usuário em uma rede social e um novo amigo, 
# e adicione esse novo amigo na lista.

def adicionar_amigo(lista_amigos, novo_amigo):
    lista_amigos.append(novo_amigo)

amigos = ['Milena', 'Maria', 'Catarina', 'Fernando']

novo_amigo = 'Pedro'
adicionar_amigo(amigos, novo_amigo)

print("Lista de amigos atualizada:", amigos)
