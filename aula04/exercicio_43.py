"""
Crie um programa onde o usuário possa digitar vários valores numéricos
e cadastre-os em uma lista.
Caso o número já exista lá dentro, ele não será adicionado.
No final, serão exibidos todos os valores únicos digitados, em ordem crescente.
"""

lista = []

while True:
    num = int(input("Digite um número: "))
    if num in lista:
        print("O número já está na lista.")
    else:
        lista.append(num)
        print("Item adicionado com sucesso")

    continuar = str(input("Quer continuar? (s/n)")).strip().upper() #strip retira os espaços e upper deixa as letras maiúsculas
    if continuar[0] == "N":
        break
    elif continuar[0] == "S":
        print("Continuando...")
    else:
        print("Digite apenas sim ou não")

lista.sort() #sort é um método para deixar itens ordenados (em ordem crescente)
print(f"Você digitou esses valores: {lista}")