"""
Crie uma classe chamada Carro que tenha o atributo marca.
Em seguida, crie um método chamado info que imprima uma
mensagem dizendo "Este carro é da marca {marca}.
"""

class Carro:
    def __init__(self, marca):
        self.marca = marca

    def info(self):
        print(f"Este carro é da marca {self.marca}")

carro1 = Carro("Chevrolet")
carro2 = Carro("Volkswagen")

carro1.info()
carro2.info()