#Suponha que em Codeville temos diferentes 
# instrumentos musicais, como violões e pianos.
# Vamos criar um exemplo de polimorfismo com 
# um método tocar compartilhado:

class IntrumentosMusicais:
    def tocar(self):
        pass

class Violao(IntrumentosMusicais):
    def tocar(self):
        print("Tocando acordes no violão")

class Piano(IntrumentosMusicais):
    def tocar(self):
        print("Tocando notas no piano")

violao = Violao()
piano = Piano()

instrumentos = [violao, piano]

for instrumento in instrumentos:
    instrumento.tocar()