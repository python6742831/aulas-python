# Um restaurante quer um sistema  para percorrer
# a lista de pedidos e calcular o valor total da conta.

#definindo uma lista de pedidos
itens_pedido = {'xbacon': 15, 'refri': 8, 'batata frita': 5}
print(itens_pedido)

#variável do valor que o cliente vai pagar
total_conta = 0

#percorreu a lista de pedido e calculou o valor total do período
for item, valor in itens_pedido.items(): #função items trabalha a interação dentro de um objeto
    total_conta += valor
print("O valor total da conta é de R$ {:.2f}".format((total_conta)))
