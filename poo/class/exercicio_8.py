"""
Crie uma classe chamada Aluno que tenha os 
atributos nome e nota. Em seguida, crie um 
método chamado nota e imprima a nota do aluno
"""

class Aluno:
    def __init__(self, nome, nota):
        self.nome = nome
        self.nota = nota

    def notaAluno(self):
        print(f"A nota final do aluno: {self.nome} é: {self.nota}")

aluno1 = Aluno("Rafael", 7.5)
aluno2 = Aluno("Ana Carolina", 9.1)

aluno1.notaAluno()
aluno2.notaAluno()