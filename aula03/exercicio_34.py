""""
Crie um programa que verifica se uma pessoa pode se aposentar.
Para se aposentar, é preciso ter mais de 60 anos 
ou ter mais de 30 anos de contribuição ao INSS.
"""

idade = int(input("Qual a sua idade? \n"))
contribuicao = int(input("Quanto tempo você tem de contribuição ao INSS? \n"))

if idade >= 60 or contribuicao >= 30:
    print("Parabéns, você já pode se aposentar.")
else:
    print("Infelizmente você ainda não possui os requisitos minímos para se aposentar.")