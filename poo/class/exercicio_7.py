"""
Crie uma classe chamada Ponto que tenha os atributos x e y.
Em seguida, crie um método chamado mostrar_coordenadas que
imprima as coordenadas do ponto.
"""

class Ponto:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def mostrar_coordenadas(self):
        print(f"Coordenadas: x = {self.x} e y = {self.y}")

coordenada1 = Ponto(24353, -3682)
coordenada2 = Ponto(98482237, 16383582)

coordenada1.mostrar_coordenadas()
coordenada2.mostrar_coordenadas()