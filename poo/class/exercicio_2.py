"""
Crie uma classe chamada Retangulo que tenha os atributos
largura e altura. Em seguida, crie um método chamado
area que retorne a área do retângulo (área = largura * altura)
"""

class Retangulo:
    def __init__(self, largura, altura):
        self.largura = largura
        self.altura = altura

    def area(self):
        area = self.largura * self.altura
        print(f"Largura do retângulo: {self.largura}, Altura do retângulo: {self.altura}, Área do retângulo: {area}")

retangulo1 = Retangulo(4, 2)
retangulo2 = Retangulo(10, 5)

retangulo1.area()
retangulo2.area()