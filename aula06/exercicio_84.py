#Crie um módulo chamado "geometria" que contenha funções 
# para calcular a área e o perímetro de um retângulo. 
# Em seguida, crie um programa principal que importe esse módulo 
# e peça ao usuário para digitar a largura e a altura do retângulo. 
# O programa deve imprimir a área e o perímetro.

from modulos import geometria

largura = float(input("Digite a largura do retângulo: "))
altura = float(input("Digite a altura do triângulo: "))

print(f"A área do retângulo é igual a {geometria.areaRetangulo(largura, altura)} e o perimêtro é {geometria.perimetroRetangulo(largura, altura)}")