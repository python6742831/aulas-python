# Crie um módulo chamado "operacoes_matematicas" que
# contenha funções para calcular a potência e
# a raiz quadrada de um número. Em seguida,
# crie um programa principal que importe esse
# módulo e peça ao usuário para digitar um número
# e a operação desejada (potência ou raiz quadrada).
# O programa deve imprimir o resultado da operação.

from modulos import operacoes_matematicas

operacao = input("Digite a operação desejada (** ou r(raiz quadrada)): ")

if operacao == "**":
    numero = int(input("Digite um número: "))
    potencia = int(input("Digite a potência que deseja: "))
    print(f"A potência de {numero} é igual a {operacoes_matematicas.potencia(numero, potencia)}")
elif operacao == "r":
    numero = int(input("Digite um número: "))
    print(f"A raiz quadrada de {numero} é igual a {operacoes_matematicas.raizQuadrada(numero)}")
else:
    print("Operação matemática inválida!!")