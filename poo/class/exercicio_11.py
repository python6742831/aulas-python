"""
Crie uma classe chamada ContaBancaria que tenha os
atributos titular e saldo. Em seguida, crie um 
método chamado mostrar_saldo que imprima o saldo da conta.
"""
titular = input("Digite o nome do titular da conta bancária: ")
saldo = float(input("Digite a quantia presente na conta: "))

class ContaBancaria:
    def __init__(self, titular, saldo):
        self.titular = titular
        self.saldo = saldo

    def mostrar_saldo(self):
        print(f"O saldo da conta bancária do titular: {self.titular} é equilente a R$ {self.saldo:.2f}")

conta = ContaBancaria(titular, saldo)
conta.mostrar_saldo()