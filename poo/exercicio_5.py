#No zoológico de Codeville, 
# existem diferentes animais com 
# diferentes características. 
# Crie uma classe base Animal com os 
# atributos nome e especie. Crie subclasses
#  Mamifero e Ave que herdam da classe base e 
# adicionam um atributo específico a cada uma.

class Animal():
    def __init__(self, nome, especie):
        self.nome = nome
        self.especie = especie

class Mamifero(Animal):
    def __init__(self, nome, especie, pelo):
        super().__init__(nome, especie)
        self.pelo = pelo

class Ave(Animal):
    def __init__(self, nome, especie, penas):
        super().__init__(nome, especie)
        self.penas = penas

mamifero = Mamifero("Leão", "Felino", "Marrom")
ave = Ave("Águia", "Ave de Rapina", "Marrom e Branco")

print(f"Nome do Mamífero: {mamifero.nome}")
print(f"Espécie do Mamífero: {mamifero.especie}")
print(f"Cor do Pelo: {mamifero.pelo}")
print("-------------------------------")
print(f"Nome da Ave: {ave.nome}")
print(f"Espécie da Ave: {ave.especie}")
print(f"Cor das Penas: {ave.penas}")