#No mundo dos cachorros, há diferentes 
# raças com características distintas. 
# Crie uma classe base Cachorro com 
# o atributo nome. Crie subclasses 
# Golden Retriever e Poodle que 
# herdam da classe base e adicionam
#  atributos específicos.

class Cachorro:
    def __init__(self, nome):
        self.nome = nome

class GoldenRetriever(Cachorro):
    def __init__(self, nome, tamanho, pelo):
        super().__init__(nome)
        self.tamanho = tamanho
        self.pelo = pelo

class Poodle(Cachorro):
    def __init__(self, nome, tamanho, pelo):
        super().__init__(nome)
        self.tamanho = tamanho
        self.pelo = pelo

goldenRetriever = GoldenRetriever("Bob", "porte grande", "liso e longos")
poodle = Poodle("Max", "porte pequeno", "enrolado e curto")

print(f"Nome da Cachorro: {goldenRetriever.nome}")
print(f"Tamanho do Cachorro: {goldenRetriever.tamanho}")
print(f"Pelagem do Cachorro: {goldenRetriever.pelo}")

print("---------------------------------")
print(f"Nome do Cachorro: {poodle.nome}")
print(f"Tamanho do Cachorro: {poodle.tamanho}")
print(f"Pelagem do Cachorro: {poodle.pelo}")