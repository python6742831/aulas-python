#Na família de bonecas da Barbie, 
# existem diferentes membros com 
# características únicas. Crie uma 
# classe base Boneca com o atributo nome. 
# Crie subclasses Barbie e Ken que herdam da 
# classe base e adicionam atributos específicos.

class Boneca:
    def __init__(self, nome):
        self.nome = nome

class Barbie(Boneca):
    def __init__(self, nome, mulher):
        super().__init__(nome)
        self.mulher = mulher

class Ken(Boneca):
    def __init__(self, nome, homem):
        super().__init__(nome)
        self.homem = homem

barbie = Barbie("Barbie", "Feminino")
ken = Ken("Ken", "Masculino")

print(f"Nome da Boneca: {barbie.nome}")
print(f"Sexo da Boneca: {barbie.mulher}")
print("---------------------------------")
print(f"Nome do Boneco: {ken.nome}")
print(f"Sexo do Boneco: {ken.homem}")