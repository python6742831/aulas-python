#Um aplicativo pode usar um laço de repetição "for" 
#para percorrer a lista de usuários e enviar uma notificação para cada um.

usuarios = ['Ana', 'João', 'Pedro', 'Maria']
mensagem = "Temos desconto amanhã 30/04"

for usuario in usuarios: #in é usado para acessar dados de um array
    print("{}, {}".format(usuario, mensagem))