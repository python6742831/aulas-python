"""
Crie uma classe chamada Pessoa que tenha o atributo nome.
Em seguida, cire um método chamado apresentar que imprima
uma mensagem dizendo "Olá, eu sou {nome}."
"""

class Pessoa:
    def __init__(self, nome):
        self.nome = nome

    def apresentar(self):
        print(f"Olá, eu sou {self.nome}")

pessoa1 = Pessoa("Milena")
pessoa2 = Pessoa("Lucas")

pessoa1.apresentar()
pessoa2.apresentar()