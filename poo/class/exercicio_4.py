"""
Cire uma classe chamada Calculadora que tenha
os métodos somar, subtrair, multiplicar e dividir.
Cada método deve receber dois primeiros números como
parâmetros e retornar o resultado da operação correspondente.
"""

class Calculadora:
    def __init__(self, num1, num2):
        self.num1 = num1
        self.num2 = num2

    def somar(self):
        return self.num1 + self.num2
    def subtrair(self):
        return self.num1 - self.num2
    def multiplicar(self):
        return self.num1 * self.num2
    def dividir(self):
        return self.num1 / self.num2

numeros = Calculadora(8, 2)
print(numeros.somar())
print(numeros.subtrair())
print(numeros.multiplicar())
print(numeros.dividir())