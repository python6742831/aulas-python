def areaRetangulo(base, altura):
    return base * altura

def perimetroRetangulo(base, altura):
    return 2 * (base + altura) 