#Em uma loja online chamada 
# ShopCodeville, você está criando 
# um sistema de produtos. Crie uma 
# classe Produto com os atributos nome, 
# preco e descricao. Crie um método exibir_detalhes para 
# mostrar as informações do produto.

class Produto:
    def __init__(self, nome, preco, descricao):
        self.nome = nome
        self.preco = preco
        self.descricao = descricao

    def exibir_detalhes(self):
        print(f"Produto: {self.nome}")
        print(f"Valor do Produto: R${self.preco:.2f}")
        print(f"Descricao: {self.descricao}")

produto = Produto("Camiseta", 20, "Camiseta de algodão")
produto.exibir_detalhes()