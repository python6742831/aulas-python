#Em Codeville, temos diferentes tipos de alunos na 
# escola, mas nos concentramos nos atributos essenciais, 
# como nome e idade, enquanto ocultamos os detalhes internos.

class Aluno:
    def __init__(self, nome, idade, matricula, turma):
        self.nome = nome
        self.idade = idade
        self.matricula = matricula
        self.turma = turma

    def apresentar(self):
        print(f"Oie, meu nome é {self.nome}, tenho {self.idade} anos e sou da turma {self.turma}")

class Turma:
    def __init__(self, nome, serie):
        self.nome = nome
        self.serie = serie
        self.alunos = []

    def adicionar_alunos(self, aluno):
        self.alunos.append(aluno)

    def listar_alunos(self):
        print(f"Aluno da turma {self.nome} - Série {self.serie}")
        for aluno in self.alunos:
            print(f"Nome: {aluno.nome}, Matrícula: {aluno.matricula}")

#Criando objetos das classes
turmaA = Turma("Turma A", 8)
aluno1 = Aluno("João", 13, "72827", turmaA)
aluno2 = Aluno("Giovana", 13, "25252", turmaA)

#Adicionando alunos na Turma
turmaA.adicionar_alunos(aluno1)
turmaA.adicionar_alunos(aluno2)

#Apresentação dos alunos
"""for aluno in turmaA.alunos:
    aluno.apresentar()"""

turmaA.listar_alunos()