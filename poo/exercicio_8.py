#No zoológico de Codeville, 
# diferentes animais emitem sons diferentes. 
# Crie uma classe base Animal com o método emitir_som. 
# Crie subclasses Cachorro e Gato que herdam da 
# classe base e sobrescrevem o método para emitir
#  sons característicos.

class Animal:
    def emitir_som(self):
        pass

class Cachorro(Animal):
    def emitir_som(self):
        print("O cachorro latiu")

class Gato(Animal):
    def emitir_som(self):
        print(f"O gato miou")

cachorro = Cachorro()
gato = Gato()

sons = [cachorro, gato]

for som in sons:
    som.emitir_som()