#Barbie guarda suas receitas secretas. 
# Crie uma classe Receita com o atributo conteudo. 
# Crie um método mostrar que exibe o conteúdo 
# da receita se a senha correta for inserida.

class Receita:
    def __init__(self, conteudo, senha_correta):
        self.__conteudo = conteudo
        self.__senha_correta = senha_correta

    def mostrar(self, senha_inserida):
        if senha_inserida == self.__senha_correta:
            print("Senha correta, agora você tem acesso ao conteúdo das receitas")
            print(f"{self.__conteudo}")
        else:
            print("Senha incorreta")

senha_correta = "1234"
receita = Receita("Bolo de Chocolate", senha_correta)
senha_inserida = input("Digite a senha: ")
receita.mostrar(senha_inserida)












