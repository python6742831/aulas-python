"""
Suponha que você tenha uma lista com as 
idades de todos os alunos de uma turma, e deseja 
encontrar a média de idade
"""

idades = (18, 19, 20, 21, 22)

media_idade = sum(idades) / len(idades)   #sum = soma os números, len = conta a quantidade de itens

print(f"A média da idade dos alunos é: {media_idade}")