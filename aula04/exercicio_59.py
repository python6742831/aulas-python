"""
Uma loja de roupas precisa gerenciar seu estoque. 
Escreva um programa em Python que permita ao usuário adicionar 
novos itens ao estoque e verificar a quantidade disponível de cada item.
 O programa deve permitir que o usuário adicione um novo item especificando 
 seu nome, preço e quantidade inicial. O usuário também deve 
 poder verificar a quantidade disponível de um item existente.
"""

relatorio_vendas = {}

while True:
    produto = input("Digite o nome do produto ou digite sair para encerrar: ")
    if produto == "sair":
        break

    quantidade = int(input("Digite a quantidade vendida do produto: "))
    relatorio_vendas[produto] = quantidade

print("Relatório de vendas")

for produto, quantidade in relatorio_vendas.items():
    print(f"{produto}: {quantidade} unidades vendidas")