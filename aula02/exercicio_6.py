#Faça um programa que verifique se a letra digitada é vogal ou consoante

letra = input("Digite uma letra: \n").upper()  #upper() deixa todas as letras maiúsculas

if letra == "A" or letra == "E" or letra == "I" or letra == "O" or letra == "U":
    print("É vogal")
else:
    print("É consoante")