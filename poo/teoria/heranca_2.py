#Imagine que em Codeville temos uma obediência 
# de animais, onde existem animais terrestres e 
# animais aquáticos. Vamos criar essa autoridade 
# usando herança:

class Animal:
    def __init__(self, nome, habitat):
        self.nome = nome
        self.habitat = habitat

class AnimalTerrestre(Animal):
    def mover(self):
        print(f"O animal {self.nome} está se movendo na rua")

class AnimalAquatico(Animal):
    def nadar(self):
        print(f"O animal {self.nome} está nadando no rio")

cachorro = AnimalTerrestre("Dog", "terrestre")
peixe = AnimalAquatico("peixe", "aquático")

cachorro.mover()
peixe.nadar()