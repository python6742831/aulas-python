#Crie um programa em Python que calcule a 
# média de likes por postagem em um perfil do 
# Facebook, utilizando um loop while.

posts = [120, 200, 95, 150, 300]

soma = 0
tamanho = 0

while tamanho < len(posts):
    soma+= posts[tamanho]
    tamanho += 1

print(f"Média de likes: {soma/tamanho}")