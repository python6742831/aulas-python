# Na empresa Codeville Corp., os dados
# dos funcionários devem ser protegidos.
# Crie uma classe Funcionario com os atributos
# nome, cargo e salario. Mantenha o salário
# privado e adicione um método aumentar_salario para
# alterá-lo.

class Funcionario:
    def __init__(self, nome, cargo, salario):
        self.nome = nome
        self.cargo = cargo
        self.__salario = salario

    def ver_salario(self):
        print(f"Seu salário é R${self.__salario:.2f}")

    def aumentar_salario(self, salario_antigo, salario_novo):
        if salario_antigo == self.__salario:
            self.__salario = salario_novo
        else:
            print("Erro")

funcionario = Funcionario("Milena", "Desenvolvedora Front-end", 1000)
funcionario.ver_salario()
funcionario.aumentar_salario(1000, 3000)
funcionario.ver_salario()