"""
Crie uma classe chamada Cachorro que tenha o atributo nome.
Em seguida, crie um método chamado latir que imprima "Au au!"
"""

class Cachorro:
    def __init__(self, nome):
        self.nome = nome

    def latir(self):
        print(f"O cachorro {self.nome} latiu: Au au!")

cachorro1 = Cachorro("Mili")
cachorro2 = Cachorro("Bob")

cachorro1.latir()
cachorro2.latir()