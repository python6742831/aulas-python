"""
Crie uma classe chamda Circulo que tenha o atributo raio.
Em seguida, crie um método chamado area que retorne a área
do círculo (área = pi * raio^2)
"""

class Circulo:
    def __init__(self, raio):
        self.raio = raio

    def areaCirculo(self):
        area = 3.14 * (self.raio ** 2)
        print(f"A área do círculo é igual a: {area}")

raio1 = Circulo(2)
raio2 = Circulo(10)

raio1.areaCirculo()
raio2.areaCirculo()