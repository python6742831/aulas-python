"""
crie uma classe chamada Animal que tenha
o atributo nome. Em seguida, crie um método
chamado emitir_som que imprima "o {nome} emitiu um som"
"""

class Animal: 
    def __init__(self, nome):
        self.nome = nome

    def emitir_som(self):
        print(f"O {self.nome} emitiu um som")

animal1 = Animal("Cachorro")
animal2 = Animal("Gato")

animal1.emitir_som()
animal2.emitir_som()