#Oppenheimer é um cinéfilo e possui diversos DVDs 
# em sua coleção. Crie uma classe base 
# Filme com o atributo titulo. 
# Crie subclasses Comedia e Acao que herdam da 
# classe base e adicionam atributos específicos.

class Filme:
    def __init__(self, titulo):
        self.titulo = titulo

class Comedia(Filme):
    def __init__(self, titulo, engracado):
        super().__init__(titulo)
        self.engracado = engracado

class Acao(Filme):
    def __init__(self, titulo, perseguicao):
        super().__init__(titulo)
        self.perseguicao = perseguicao

comedia = Comedia("Minha Mãe é uma Peça", "Engraçado")
acao = Acao("Velozes e Furioso", "Cenas de Perseguição")

print(f"Titulo da Filme: {comedia.titulo}")
print(f"Característica do Filme: {comedia.engracado}")
print("---------------------------------")
print(f"Titulo do Filme: {acao.titulo}")
print(f"Característica do Filme: {acao.perseguicao}")