"""
Crie um programa que vai ler vários números e colocá-los em uma lista
Depois disso, mostre:
a) quantos números foram digitados.
b) a lista de valores, ordenada de forma decrescente
c) se o valor 5 foi digitado e está ou não na lista.
"""

lista = []

while 1:
    lista.append(int(input("Digite um número: ")))
    continuar = str(input("Quer continuar? ")).strip().upper()
    if continuar[0] == "S":
        print("Continuando...")
    elif continuar[0] == "N":
        print("Programa finalizado")
        break
    else:
        print("Digite uma resposta válida (s/n)")

print(f"{len(lista)} números foram digitados.") #len é usado para contar quantos itens existem
lista.sort(reverse=True) #sort é usado para exibir os itens em ordem crescente e o reverse foi utlizado para deixar em ordem decrescente
print(f"Essa é a lista em ordem decrescente {lista}")

if 5 in lista:
    print("O 5 foi digitado")
else:
    print("O 5 não foi digitado")
