#Oppenheimer está construindo cofres 
 # de segurança. Crie uma classe Cofre com 
 # o atributo codigo. Defina um método 
 # abrir para verificar 
 # se o código inserido está correto.

class Cofre:
    def __init__(self, codigo):
        self.__codigo = codigo

    def abrir(self, codigo_inserido):
        if codigo_inserido == self.__codigo:
            print("Cofre aberto")
        else:
            print("Código incorreto")

codigo = Cofre("45678")
codigo_inserido = input("Digite o código do cofre: ")
codigo.abrir(codigo_inserido)