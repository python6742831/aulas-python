import math

def potencia(num, potencia):
    return num ** potencia

def raizQuadrada(num):
    return math.sqrt(num)