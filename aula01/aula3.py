import os
#limpar a tela do terminal
os.system("cls")
print("-----------------------------")

nome = input("Digite seu nome: ")
salario = input("Digite seu salário: ")
aumento = input("Digite seu aumento: ")
print("------------------------------")

aumento = float(aumento)
salario = float(salario)

aumento = salario * aumento / 100
print("O aumento é de:", aumento)
salario = salario + aumento
print("O salário será de:", salario)
print("----------------------------")

print("Olá {0}, seu aumento é de {1:.2f}, e seu novo salário é de {2} ".format(nome, aumento, salario))