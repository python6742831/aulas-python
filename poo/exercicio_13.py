#Barbie está fazendo compras online.
#  Crie uma classe Compra com o atributo valor.
#  Implemente um método efetuar_pagamento que atualiza 
# o valor após o pagamento.

class Compra:
    def __init__(self, valor):
        self.valor = valor

    def efetuar_pagamento(self, valor_pago):
        if valor_pago >= self.valor:
            print("Pagamento completo.")
        else:
            print("Valor insuficiente. O pagamento não pôde ser concluído.")

compra = Compra(100)
valor_pago = float(input(f"Digite o valor pago: R$"))
compra.efetuar_pagamento(valor_pago)

print(f"Valor restante não pago: R${compra.valor - valor_pago:.2f}")