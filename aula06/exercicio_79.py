# Crie um módulo chamado "conversor" 
# que contenha duas funções: uma para 
# converter graus Celsius para Fahrenheit e 
# outra para converter graus Fahrenheit para Celsius.
#  Em seguida, crie um programa principal que importe 
#  esse módulo e peça ao usuário para digitar uma temperatura 
#  e a unidade de medida (C ou F). O programa deve imprimir a 
#  temperatura convertida para a outra unidade.

from modulos import conversor

temperatura = float(input("Digite uma temperatura: "))
unidade = input("Qual a unidade de medida (C ou F): ")

if unidade.upper()  == "C":
    resultado = conversor.celsius(temperatura)
elif unidade.upper()  == "F":
    resultado = conversor.fahrenheit(temperatura)
else: 
    resultado = "Unidade não reconhecida"
    print("Unidade de medida não identificada")

print(f'temperatura transmitida em {unidade}, é igual a, {resultado}')