#Desenvolva um programa em Python que exiba os primeiros 
# 10 números primos encontrados em uma lista de amigos do Facebook.

def is_prime(num): #def declara uma função
    if num < 2:
        return False
    for i in range(2, int(num ** 0.5)+1):
        if num % i == 0:
            return False
    return True

friends_list = [2, 3, 5, 6, 8, 10, 11, 13, 15, 17, 19, 21]

count = 0

for friend in friends_list:
    if is_prime(friend):
        print(friend)
        count += 1
    if count == 10:
        break