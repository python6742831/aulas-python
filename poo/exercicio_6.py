#Na empresa Codeville Corp.,
# existem diferentes níveis de 
# funcionários. Crie uma classe base 
# Funcionario com os atributos nome e cargo. 
# Crie subclasses Gerente e Atendente que herdam da 
# classe base e adicionam um atributo específico 
# a cada uma

class Funcionario:
    def __init__(self, nome, cargo):
        self.nome = nome
        self.cargo = cargo

class Gerente(Funcionario):
    def __init__(self, nome, cargo, administrar):
        super().__init__(nome, cargo)
        self.administrar = administrar

class Atendente(Funcionario):
    def __init__(self, nome, cargo, atender):
        super().__init__(nome, cargo)
        self.atender = atender

gerente = Gerente("Paulo", "Gerente de atendimento", "Admnistrar a equipe de atendimento")
atendente = Atendente("Gustavo", "Atendente", "Atender os clientes")

print(f"Nome do Funcionário: {gerente.nome}")
print(f"Cargo do Funcionário: {gerente.cargo}")
print(f"Função do Funcionário: {gerente.administrar}")
print("---------------------------------")
print(f"Nome do Funcionário: {atendente.nome}")
print(f"Cargo do Funcionário: {atendente.cargo}")
print(f"Função do Funcionário: {atendente.atender}")