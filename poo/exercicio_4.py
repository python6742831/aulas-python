#Na cidade de Codeville, 
# há diversos tipos de veículos. 
# Crie uma classe Veiculo com os atributos 
# marca, modelo e ano. 
# Crie um método mostrar_infopara 
# exibir as informações do veículo.

class Veiculo:
    def __init__(self, marca, modelo, ano):
        self.marca = marca
        self.modelo = modelo
        self.ano = ano

    def exibir_informacoes(self):
        print(f"Marca do Veículo: {self.marca}")
        print(f"Modelo do Veículo: {self.modelo}")
        print(f"Ano do Veículo: {self.ano}")

veiculo = Veiculo("Chevrolet", "Prisma", 2017)
veiculo.exibir_informacoes()