#Crie uma função chamada `calcular_idade` que receba o
#  ano de nascimento de uma pessoa e retorne sua idade.

def calcular_idade(ano):
    if(ano > 2023):
        print("Ano inválido")
    else:
        idade = 2023 - ano
        print(idade)

calcular_idade(2005)