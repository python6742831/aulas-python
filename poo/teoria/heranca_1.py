#Imagine que em Codeville, temos uma
# hierarquia de prédios. Existem prédios básicos, 
# como "PrédioResidencial" e "PrédioComercial". 
# Cada um deles herda algumas características da classe 
# "Prédio" e adiciona suas próprias particularidades.

class Predio:
    def __init__(self, endereco, andares):
        self.endereco = endereco
        self.andares = andares

class PredioResidencial(Predio):
    def __init__(self, endereco, andares, numero_apartamento):
        super().__init__(endereco, andares) #chamando o construtor da classe pai
        self.numero_apartamento = numero_apartamento

class PredioComercial(Predio):
    def __init__(self, endereco, andares, numero_salas):
        super().__init__(endereco, andares)
        self.numero_salas = numero_salas

predio_residencial = PredioResidencial("Rua B, 123", 8, 40)
predio_comercial = PredioComercial("Rua A, 456", 4, 10)

print("Informações do Prédio Residencial:")
print(f"Endereço: {predio_residencial.endereco}")
print(f"N° de andares: {predio_residencial.andares}")
print(f"N° de apartamentos: {predio_residencial.numero_apartamento}")

print("Informações do Prédio Comercial:")
print(f"Endereço: {predio_comercial.endereco}")
print(f"N° de andares: {predio_comercial.andares}")
print(f"N° de salas: {predio_comercial.numero_salas}")